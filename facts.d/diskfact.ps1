$disk = Get-WmiObject Win32_LogicalDisk  -Filter "DeviceID='C:'" |
Select-Object Size,FreeSpace

Write-Host -NoNewline "diskcapacity="
Write-host  ($disk.Size/1024/1024/1024)"GB"
Write-Host -NoNewline "freediskspace="
Write-Host ($disk.FreeSpace / 1024 /1024 / 1024) "GB"